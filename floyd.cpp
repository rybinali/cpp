#include "floyd.h"

#include <iostream>
#include <vector>
#include <limits>

using namespace std;

vector<int> floydWarshall(const vector<vector<int>> &graph, int source, int destination) {
    const int VERTEX_COUNT = graph.size();
    const int INF = numeric_limits<int>::max();

    vector<vector<int>> dist(VERTEX_COUNT, vector<int>(VERTEX_COUNT, INF));

    vector<vector<int>> from(VERTEX_COUNT, vector<int>(VERTEX_COUNT, -1));

    for (int i = 0; i < VERTEX_COUNT; i++) {
        for (int j = 0; j < VERTEX_COUNT; j++) {
            dist[i][j] = graph[i][j];
            if (graph[i][j] != INF) {
                from[i][j] = j;
            }
        }
    }

    for (int v = 0; v < VERTEX_COUNT; v++) {
        for (int i = 0; i < VERTEX_COUNT; i++) {
            for (int j = 0; j < VERTEX_COUNT; j++) {
                if (dist[i][v] != INF && dist[v][j] != INF && dist[i][v] + dist[v][j] < dist[i][j]) {
                    dist[i][j] = dist[i][v] + dist[v][j];
                    from[i][j] = from[i][v];
                }
            }
        }
    }

    if (dist[source][destination] == INF) {
        throw runtime_error("The path does not exist!");
    }


    vector<int> path;

    int v = source;
    while (v != destination) {
        path.push_back(v);
        v = from[v][destination];
    }
    path.push_back(v);

    return path;
}
