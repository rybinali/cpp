#include "floyd.h"
#include <iostream>
#include <vector>
#include <limits>
#include <thread>
#include <mutex>

using namespace std;

void updateDistances(const vector<vector<int>>& graph,
                     vector<vector<int>>& dist,
                     vector<vector<int>>& from,
                     int v, int start, int end, mutex& mtx) {
    const int VERTEX_COUNT = graph.size();
    const int INF = numeric_limits<int>::max();

    for (int i = start; i < end; i++) {
        for (int j = 0; j < VERTEX_COUNT; j++) {
            if (dist[i][v] != INF && dist[v][j] != INF && dist[i][v] + dist[v][j] < dist[i][j]) {
                lock_guard<mutex> lock(mtx);
                dist[i][j] = dist[i][v] + dist[v][j];
                from[i][j] = from[i][v];
            }
        }
    }
}

vector<int> floydWarshallMT(const vector<vector<int>>& graph, int source, int destination) {
    const int VERTEX_COUNT = graph.size();
    const int INF = numeric_limits<int>::max();

    vector<vector<int>> dist(VERTEX_COUNT, vector<int>(VERTEX_COUNT, INF));
    vector<vector<int>> from(VERTEX_COUNT, vector<int>(VERTEX_COUNT, -1));

    for (int i = 0; i < VERTEX_COUNT; i++) {
        for (int j = 0; j < VERTEX_COUNT; j++) {
            dist[i][j] = graph[i][j];
            if (graph[i][j] != INF) {
                from[i][j] = j;
            }
        }
    }

    mutex mtx;
    int num_threads = thread::hardware_concurrency();
    vector<thread> threads(num_threads);

    for (int v = 0; v < VERTEX_COUNT; v++) {
        int chunk_size = VERTEX_COUNT / num_threads;
        for (int t = 0; t < num_threads; t++) {
            int start = t * chunk_size;
            int end = (t == num_threads - 1) ? VERTEX_COUNT : start + chunk_size;
            threads[t] = thread(updateDistances, ref(graph), ref(dist), ref(from), v, start, end, ref(mtx));
        }
        for (auto& t : threads) {
            if (t.joinable()) {
                t.join();
            }
        }
    }

    if (dist[source][destination] == INF) {
        throw runtime_error("The path does not exist!");
    }

    vector<int> path;
    int v = source;
    while (v != destination) {
        path.push_back(v);
        v = from[v][destination];
    }
    path.push_back(v);

    return path;
}

