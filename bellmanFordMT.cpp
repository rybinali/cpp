#include <vector>
#include <thread>
#include <limits>
#include <stdexcept>
#include <algorithm>
#include "bellmanFord.h"

using namespace std;

const int INF = numeric_limits<int>::max();

void relaxEdges(const vector<vector<int>> &graph,
                vector<int> &dist,
                vector<int> &from,
                int start, int end, mutex &mtx) {
    int VERTEX_COUNT = graph.size();
    for (int u = start; u < end; ++u) {
        for (int v = 0; v < VERTEX_COUNT; ++v) {
            if (graph[u][v] != INF && dist[u] != INF) {
                int newDistance = dist[u] + graph[u][v];
                if (newDistance < dist[v]) {
                    lock_guard<mutex> lock(mtx);
                    dist[v] = newDistance;
                    from[v] = u;
                }
            }
        }
    }
}

vector<int> bellmanFordMT(const vector<vector<int>> &graph, int source, int destination) {
    int VERTEX_COUNT = graph.size();
    vector<int> dist(VERTEX_COUNT, INF);
    vector<int> from(VERTEX_COUNT, -1);
    dist[source] = 0;

    mutex mtx;
    int num_threads = thread::hardware_concurrency();
    vector<thread> threads(num_threads);

    for (int i = 0; i < VERTEX_COUNT - 1; ++i) {
        int chunk_size = VERTEX_COUNT / num_threads;
        for (int t = 0; t < num_threads; ++t) {
            int start = t * chunk_size;
            int end = (t == num_threads - 1) ? VERTEX_COUNT : start + chunk_size;
            threads[t] = thread(relaxEdges, ref(graph), ref(dist), ref(from), start, end, ref(mtx));
        }
        for (auto &t: threads) {
            if (t.joinable()) {
                t.join();
            }
        }
    }

    // Check for negative-weight cycles
    for (int u = 0; u < VERTEX_COUNT; ++u) {
        for (int v = 0; v < VERTEX_COUNT; ++v) {
            if (graph[u][v] != INF && dist[u] != INF && dist[u] + graph[u][v] < dist[v]) {
                throw runtime_error("Graph contains a negative-weight cycle");
            }
        }
    }

    if (dist[destination] == INF) {
        throw runtime_error("The path does not exist!");
    }

    vector<int> path;
    for (int v = destination; v != -1; v = from[v]) {
        path.push_back(v);
    }

    reverse(path.begin(), path.end());

    return path;
}

