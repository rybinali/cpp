#include <iostream>
#include <vector>
#include <chrono>
#include <limits>
#include <cstring>

#include "floyd.h"
#include "bellmanFord.h"


namespace timer {
    auto now() {
        return std::chrono::high_resolution_clock::now();
    }

    template<typename TimePoint>
    long long to_us(TimePoint tp) {
        return std::chrono::duration_cast<std::chrono::microseconds>(tp).count();
    }
}

void print_path(const std::vector<int> &path) {
    for (size_t i = 0; i < path.size(); ++i) {
        std::cout << path[i];
        if (i != path.size() - 1) {
            std::cout << " -> ";
        }
    }
    std::cout << std::endl;
}

void testShortestPathAlgorithms(const std::vector<std::vector<int>> &graph, int source) {
    const int destination = graph.size() - 1;

    auto start = timer::now();
    std::vector<int> path1;
    try {
        path1 = floydWarshall(graph, source, destination);
        print_path(path1);
    } catch (const std::runtime_error &e) {
        std::cerr << "Floyd-Warshall error: " << e.what() << std::endl;
    }
    auto stop = timer::now();
    auto durationFloyd = timer::to_us(stop - start);
    std::cout << "Floyd-Warshall: ";
    std::cout << "Time: " << durationFloyd << " us\n";

    start = timer::now();
    std::vector<int> path2;
    try {
        path2 = bellmanFord(graph, source, destination);
        print_path(path2);
    } catch (const std::runtime_error &e) {
        std::cerr << "Bellman-Ford error: " << e.what() << std::endl;
    }
    stop = timer::now();
    auto durationBellman = timer::to_us(stop - start);
    std::cout << "Bellman-Ford: ";
    std::cout << "Time: " << durationBellman << " us\n";

    start = timer::now();
    std::vector<int> path3;
    try {
        path3 = floydWarshallMT(graph, source, destination);
        print_path(path3);
    } catch (const std::runtime_error &e) {
        std::cerr << "Floyd-Warshall (MT) error: " << e.what() << std::endl;
    }
    stop = timer::now();
    auto durationFloydMT = timer::to_us(stop - start);
    std::cout << "Floyd-Warshall (MT): ";
    std::cout << "Time: " << durationFloydMT << " us\n";

    start = timer::now();
    std::vector<int> path4;
    try {
        path4 = bellmanFordMT(graph, source, destination);
        print_path(path4);
    } catch (const std::runtime_error &e) {
        std::cerr << "Bellman-Ford (MT) error: " << e.what() << std::endl;
    }
    stop = timer::now();
    auto durationBellmanMT = timer::to_us(stop - start);
    std::cout << "Bellman-Ford (MT): ";
    std::cout << "Time: " << durationBellmanMT << " us\n";
}

void print_usage(const char *exe_name) {
    std::cout << "Usage: " << exe_name << " [--help]\n";
    std::cout << "       Test shortest path algorithms on predefined graphs.\n";
    std::cout << "       --help: Display this help message.\n";
}

bool is_help(const char *argument) {
    return std::strcmp(argument, "--help") == 0;
}

int main(int argc, char **argv) {
    if (argc > 1 && is_help(argv[1])) {
        print_usage(argv[0]);
        return 0;
    }

    const int INF = std::numeric_limits<int>::max();
    std::vector<std::vector<std::vector<int>>> testGraphs = {
            {{0,   5,   INF, 10},                         {INF, 0,   3,   INF},                         {INF, INF, 0, 1},                          {INF, INF, INF, 0}},

            {{0,   2,   INF, INF, 10},                    {2,   0,   3,   INF, INF},                    {INF, 3,   0, 1,   INF},                   {INF, INF, 1,   0, 2},                      {10,  INF, INF, 2,   0}},

            {{0, 1, -1}, {INF, 0, 1}, {-1, INF, 0}},

            {{0, 1, INF}, {INF, 0, 1}, {1, INF, 0}},

            {{0, 1, 2, 3}, {1, 0, 4, 5}, {2, 4, 0, 6}, {3, 5, 6, 0}},

            {{0, 2, 5, 1, INF,         INF}, {2, 0, 3, 2, INF,                      INF}, {5, 3, 0, 3, 1,                       5}, {1, 2, 3, 0, 1,                       INF}, {               INF, INF, 1,   1,   0, 2},                 {INF, INF, 5,   INF, 2,   0}},

            {{0,   INF, INF, INF},                        {INF, 0,   INF, INF},                         {INF, INF, 0, INF},                        {INF, INF, INF, 0}},

            {{0,   1,   1,   1,   1},                     {1,   0,   INF, INF, INF},                    {1,   INF, 0, INF, INF},                   {1,   INF, INF, 0, INF},                    {1,   INF, INF, INF, 0}},

            {{0,   1,   INF, INF, INF},                   {INF, 0,   1,   INF, INF},                    {INF, INF, 0, 1,   INF},                   {INF, INF, INF, 0, 1},                      {INF, INF, INF, INF, 0}},

            {{0,   2,   3,   4,   5},                     {2,   0,   4,   5,   6},                      {3,   4,   0, 6,   7},                     {4,   5,   6,   0, 7},                      {5,   6,   7,   7,   0}},

            {{0,   10,  15,  20},                         {10,  0,   35,  25},                          {15,  35,  0, 30},                         {20,  25,  30,  0}},

            {{0,   7,   9,   INF, INF, 14},               {7,   0,   10,  15,  INF, INF},               {9,   10,  0, 11,  INF, 2},                {INF, 15,  11,  0, 6,  INF},                {INF, INF, INF, 6,   0, 9},                 {14,  INF, 2,   INF, 9,   0}},

            {{0,   8,   INF, INF, INF, INF, INF},         {8,   0,   7,   INF, INF, INF, INF},          {INF, 7,   0, 6,   INF, INF, INF},         {INF, INF, 6,   0, 5,  INF, INF},           {INF, INF, INF, 5,   0, 9,  INF},           {INF, INF, INF, INF, 9,   0, 4},           {INF, INF, INF, INF, INF, 4, 0}},

            {{0,   4,   INF, INF, INF, INF, INF, 8},      {4,   0,   8,   INF, INF, INF, INF, 11},      {INF, 8,   0, 7,   INF, 4,   INF, INF},    {INF, INF, 7,   0, 9,  14,  INF, INF},      {INF, INF, INF, 9,   0, 10, INF, INF},      {INF, INF, 4,   14,  10,  0, 2, INF},      {INF, INF, INF, INF, INF, 2, 0, 1},    {8, 11, INF, INF, INF, INF, 1, 0}},

            {{0,   4,   INF, INF, INF, INF, INF, 8, INF}, {4,   0,   8,   INF, INF, INF, INF, 11, INF}, {INF, 8,   0, 7,   INF, 4,   INF, INF, 2}, {INF, INF, 7,   0, 9,  14,  INF, INF, INF}, {INF, INF, INF, 9,   0, 10, INF, INF, INF}, {INF, INF, 4,   14,  10,  0, 2, INF, INF}, {INF, INF, INF, INF, INF, 2, 0, 1, 6}, {8, 11, INF, INF, INF, INF, 1, 0, 7}, {INF, INF, 2, INF, INF, INF, 6, 7, 0}},

            {{0,   5,   INF, 10},                         {INF, 0,   3,   INF},                         {INF, INF, 0, 1},                          {INF, INF, INF, 0}},

            {{0,   1,   INF},                             {INF, 0,   -2},                               {4,   INF, 0}},

            {{0,   1,   8,   4},                          {1,   0,   2,   6},                           {8,   2,   0, 5},                          {4,   6,   5,   0}},

            {{0,   7,   INF, INF},                        {7,   0,   1,   INF},                         {INF, 1,   0, 2},                          {INF, INF, 2,   0}},

            {{INF, INF, INF},                             {INF, INF, INF},                              {INF, INF, INF}},

            {{0,   3,   INF, INF, INF, 8},                {INF, 0,   3,   INF, INF, INF},               {INF, INF, 0, 1,   INF, INF},              {INF, INF, INF, 0, 2,  INF},                {INF, -4,  INF, INF, 0, 5},                 {INF, INF, INF, 6,   INF, 0}},

            {{0,   2,   6,   INF, INF, INF},              {2,   0,   INF, 5,   INF, INF},               {6,   INF, 0, 8,   INF, INF},              {INF, 5,   8,   0, 10, 2},                  {INF, INF, INF, 10,  0, 3},                 {INF, INF, INF, 2,   3,   0}}
    };

    for (size_t i = 0; i < testGraphs.size(); ++i) {
        std::cout << "Testing graph " << i + 1 << std::endl;
        testShortestPathAlgorithms(testGraphs[i], 0); // 0 is the source vertex
        std::cout << std::endl;
    }

    return 0;
}
