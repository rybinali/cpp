#include <vector>

using namespace std;

#ifndef SEMKA_BELLMANFORD_H
#define SEMKA_BELLMANFORD_H

vector<int> bellmanFord(const vector<vector<int>> &graph, int source, int destination);

vector<int> bellmanFordMT(const vector<vector<int>>& graph, int source, int destination);

#endif //SEMKA_BELLMANFORD_H
