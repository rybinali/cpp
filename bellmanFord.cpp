#include "bellmanFord.h"

#include <vector>
#include <limits>
#include <algorithm>

using namespace std;

vector<int> bellmanFord(const vector<vector<int>> &graph, int source, int destination) {
    const int VERTEX_COUNT = graph.size();
    const int INF = numeric_limits<int>::max();

    vector<int> dist(VERTEX_COUNT, INF);
    vector<int> from(VERTEX_COUNT, -1);
    dist[source] = 0;

    for (int i = 0; i < VERTEX_COUNT - 1; ++i) {
        for (int u = 0; u < VERTEX_COUNT; ++u) {
            for (int v = 0; v < VERTEX_COUNT; ++v) {
                if (graph[u][v] != INF && dist[u] != INF && dist[u] + graph[u][v] < dist[v]) {
                    dist[v] = dist[u] + graph[u][v];
                    from[v] = u;
                }
            }
        }
    }

    for (int u = 0; u < VERTEX_COUNT; ++u) {
        for (int v = 0; v < VERTEX_COUNT; ++v) {
            if (graph[u][v] != INF && dist[u] != INF && dist[u] + graph[u][v] < dist[v]) {
                throw runtime_error("Graph contains a negative-weight cycle");
            }
        }
    }

    if (dist[destination] == INF) {
        throw runtime_error("The path does not exist!");
    }

    vector<int> path;
    for (int v = destination; v != -1 ; v = from[v]) {
        path.push_back(v);
    }

    reverse(path.begin(), path.end());

    return path;
}
