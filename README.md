V kódu jsem prováděla srovnání dvou algoritmů pro hledání nejkratší cesty v grafu: Floyd-Warshall a Bellman-Ford, a to jak v jednovláknové, tak ve vícevláknové (MT - Multi-Threaded) verzi.

Výstup v terminálu ukazuje výsledky testování těchto algoritmů na různých grafech. Každý test uvádí nejkratší cestu mezi uzly a čas potřebný k výpočtu této cesty.

1. **Výsledky pro jednotlivé grafy**: Pro každý testovaný graf jsou vypsány výsledky pro oba algoritmy. Například, u prvního grafu je nejkratší cesta "0 -> 1 -> 2 -> 3" a jsou uvedeny časy pro Floyd-Warshall a Bellman-Ford v obou režimech (jednovláknový a vícevláknový).

2. **Časová efektivita**: Časy jsou uvedeny v mikrosekundách (us) a ukazují, jak dlouho každý algoritmus trval k nalezení nejkratší cesty.

3. **Detekce chyb**: V případech, kdy algoritmy narazily na chybu (například "Bellman-Ford error: Graph contains a negative-weight cycle"), je to specificky uvedeno. To znamená, že v grafu byl detekován cyklus s negativní váhou, což je situace, kdy Bellman-Fordův algoritmus není schopen najít nejkratší cestu.

4. **Porovnání výkonu jednovláknového a vícevláknového zpracování**: Porovnáváte, jak rychle algoritmy pracují v jednovláknovém režimu oproti vícevláknovému zpracování. V některých případech vícevláknové zpracování značně zlepšuje výkon, v jiných případech je rozdíl méně výrazný.

Tento test a jeho výsledky poskytují důležitou zpětnou vazbu o výkonnosti a efektivitě implementovaných algoritmů, což je klíčové pro optimalizaci a výběr vhodného algoritmu pro konkrétní typy grafů a aplikací.