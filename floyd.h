#include <vector>

using namespace std;


#ifndef SEMKA_FLOYD_H
#define SEMKA_FLOYD_H

vector<int> floydWarshall(const vector<vector<int>> &graph, int source, int destination);

vector<int> floydWarshallMT(const vector<vector<int>> &graph, int source, int destination);

#endif //SEMKA_FLOYD_H
